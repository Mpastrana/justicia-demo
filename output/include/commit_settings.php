<?php
$tdatacommit = array();
$tdatacommit[".searchableFields"] = array();
$tdatacommit[".ShortName"] = "commit";
$tdatacommit[".OwnerID"] = "";
$tdatacommit[".OriginalTable"] = "commit";


$tdatacommit[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatacommit[".originalPagesByType"] = $tdatacommit[".pagesByType"];
$tdatacommit[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatacommit[".originalPages"] = $tdatacommit[".pages"];
$tdatacommit[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatacommit[".originalDefaultPages"] = $tdatacommit[".defaultPages"];

//	field labels
$fieldLabelscommit = array();
$fieldToolTipscommit = array();
$pageTitlescommit = array();
$placeHolderscommit = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelscommit["English"] = array();
	$fieldToolTipscommit["English"] = array();
	$placeHolderscommit["English"] = array();
	$pageTitlescommit["English"] = array();
	$fieldLabelscommit["English"]["idDepartamento"] = "Id Departamento";
	$fieldToolTipscommit["English"]["idDepartamento"] = "";
	$placeHolderscommit["English"]["idDepartamento"] = "";
	$fieldLabelscommit["English"]["idPais"] = "Id Pais";
	$fieldToolTipscommit["English"]["idPais"] = "";
	$placeHolderscommit["English"]["idPais"] = "";
	if (count($fieldToolTipscommit["English"]))
		$tdatacommit[".isUseToolTips"] = true;
}


	$tdatacommit[".NCSearch"] = true;



$tdatacommit[".shortTableName"] = "commit";
$tdatacommit[".nSecOptions"] = 0;

$tdatacommit[".mainTableOwnerID"] = "";
$tdatacommit[".entityType"] = 0;
$tdatacommit[".connId"] = "prueba_at_localhost";


$tdatacommit[".strOriginalTableName"] = "commit";

	



$tdatacommit[".showAddInPopup"] = false;

$tdatacommit[".showEditInPopup"] = false;

$tdatacommit[".showViewInPopup"] = false;

$tdatacommit[".listAjax"] = false;
//	temporary
//$tdatacommit[".listAjax"] = false;

	$tdatacommit[".audit"] = false;

	$tdatacommit[".locking"] = false;


$pages = $tdatacommit[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatacommit[".edit"] = true;
	$tdatacommit[".afterEditAction"] = 1;
	$tdatacommit[".closePopupAfterEdit"] = 1;
	$tdatacommit[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatacommit[".add"] = true;
$tdatacommit[".afterAddAction"] = 1;
$tdatacommit[".closePopupAfterAdd"] = 1;
$tdatacommit[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatacommit[".list"] = true;
}



$tdatacommit[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatacommit[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatacommit[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatacommit[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatacommit[".printFriendly"] = true;
}



$tdatacommit[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatacommit[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatacommit[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatacommit[".isUseAjaxSuggest"] = true;

$tdatacommit[".rowHighlite"] = true;





$tdatacommit[".ajaxCodeSnippetAdded"] = false;

$tdatacommit[".buttonsAdded"] = false;

$tdatacommit[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacommit[".isUseTimeForSearch"] = false;


$tdatacommit[".badgeColor"] = "E07878";


$tdatacommit[".allSearchFields"] = array();
$tdatacommit[".filterFields"] = array();
$tdatacommit[".requiredSearchFields"] = array();

$tdatacommit[".googleLikeFields"] = array();
$tdatacommit[".googleLikeFields"][] = "idDepartamento";
$tdatacommit[".googleLikeFields"][] = "idPais";



$tdatacommit[".tableType"] = "list";

$tdatacommit[".printerPageOrientation"] = 0;
$tdatacommit[".nPrinterPageScale"] = 100;

$tdatacommit[".nPrinterSplitRecords"] = 40;

$tdatacommit[".geocodingEnabled"] = false;










$tdatacommit[".pageSize"] = 20;

$tdatacommit[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatacommit[".strOrderBy"] = $tstrOrderBy;

$tdatacommit[".orderindexes"] = array();


$tdatacommit[".sqlHead"] = "SELECT idDepartamento,  	idPais";
$tdatacommit[".sqlFrom"] = "FROM `commit`";
$tdatacommit[".sqlWhereExpr"] = "";
$tdatacommit[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacommit[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacommit[".arrGroupsPerPage"] = $arrGPP;

$tdatacommit[".highlightSearchResults"] = true;

$tableKeyscommit = array();
$tableKeyscommit[] = "idDepartamento";
$tdatacommit[".Keys"] = $tableKeyscommit;


$tdatacommit[".hideMobileList"] = array();




//	idDepartamento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idDepartamento";
	$fdata["GoodName"] = "idDepartamento";
	$fdata["ownerTable"] = "commit";
	$fdata["Label"] = GetFieldLabel("commit","idDepartamento");
	$fdata["FieldType"] = 200;


	
	
			

		$fdata["strField"] = "idDepartamento";

		$fdata["sourceSingle"] = "idDepartamento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idDepartamento";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=30";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacommit["idDepartamento"] = $fdata;
		$tdatacommit[".searchableFields"][] = "idDepartamento";
//	idPais
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "idPais";
	$fdata["GoodName"] = "idPais";
	$fdata["ownerTable"] = "commit";
	$fdata["Label"] = GetFieldLabel("commit","idPais");
	$fdata["FieldType"] = 200;


	
	
			

		$fdata["strField"] = "idPais";

		$fdata["sourceSingle"] = "idPais";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idPais";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=30";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacommit["idPais"] = $fdata;
		$tdatacommit[".searchableFields"][] = "idPais";


$tables_data["commit"]=&$tdatacommit;
$field_labels["commit"] = &$fieldLabelscommit;
$fieldToolTips["commit"] = &$fieldToolTipscommit;
$placeHolders["commit"] = &$placeHolderscommit;
$page_titles["commit"] = &$pageTitlescommit;


changeTextControlsToDate( "commit" );

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["commit"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["commit"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_commit()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idDepartamento,  	idPais";
$proto0["m_strFrom"] = "FROM `commit`";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idDepartamento",
	"m_strTable" => "commit",
	"m_srcTableName" => "commit"
));

$proto6["m_sql"] = "idDepartamento";
$proto6["m_srcTableName"] = "commit";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "idPais",
	"m_strTable" => "commit",
	"m_srcTableName" => "commit"
));

$proto8["m_sql"] = "idPais";
$proto8["m_srcTableName"] = "commit";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto10=array();
$proto10["m_link"] = "SQLL_MAIN";
			$proto11=array();
$proto11["m_strName"] = "commit";
$proto11["m_srcTableName"] = "commit";
$proto11["m_columns"] = array();
$proto11["m_columns"][] = "idDepartamento";
$proto11["m_columns"][] = "idPais";
$obj = new SQLTable($proto11);

$proto10["m_table"] = $obj;
$proto10["m_sql"] = "`commit`";
$proto10["m_alias"] = "";
$proto10["m_srcTableName"] = "commit";
$proto12=array();
$proto12["m_sql"] = "";
$proto12["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto12["m_column"]=$obj;
$proto12["m_contained"] = array();
$proto12["m_strCase"] = "";
$proto12["m_havingmode"] = false;
$proto12["m_inBrackets"] = false;
$proto12["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto12);

$proto10["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto10);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="commit";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_commit = createSqlQuery_commit();


	
		;

		

$tdatacommit[".sqlquery"] = $queryData_commit;



$tdatacommit[".hasEvents"] = false;

?>